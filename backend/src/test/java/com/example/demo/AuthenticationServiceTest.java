package com.example.demo;



import com.example.demo.authentication.exception.BadRequestException;
import com.example.demo.authentication.payload.AuthResponse;
import com.example.demo.authentication.payload.LoginRequest;
import com.example.demo.authentication.repository.UserRepository;
import com.example.demo.authentication.security.TokenProvider;
import com.example.demo.authentication.services.AuthService;
import com.example.demo.exceptions.BusinessLogicException;
import com.example.demo.mocks.CustomMocks;
import com.example.demo.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.client.HttpClientErrorException;

import java.util.Optional;


@Slf4j
@SpringBootTest
public class AuthenticationServiceTest {

	@Mock
	AuthenticationManager authenticationManager;

	@Mock
	TokenProvider tokenProvider;

	@Mock
	UserService userService;

	@Mock
	PasswordEncoder passwordEncoder;

	@InjectMocks
	AuthService authService;


	@Test
	void createAuthenticationTokenOk(){
		LoginRequest loginRequest = new LoginRequest("admin@demo.com", "password");

		AuthResponse authResponse = authService.authenticate(loginRequest);

		Assertions.assertEquals(authResponse.getTokenType(), "Bearer", "Compruebo creacion de token OK");
	}

	@Test
	void createAuthenticationTokenNotOk(){
		LoginRequest loginRequest = new LoginRequest("admin@demo.com", "passwordWrong");

        Mockito.when(authenticationManager.authenticate(Mockito.any(UsernamePasswordAuthenticationToken.class))).thenThrow(new BadCredentialsException("error"));

        Assertions.assertThrows(BadRequestException.class, () -> {
            authService.authenticate(loginRequest);
        });

	}

}
