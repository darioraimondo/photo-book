package com.example.demo;


import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Optional;


@Slf4j
@SpringBootTest
public class UserRegisteredServiceTest {
/*
	@InjectMocks
	UserRegisteredService userRegisteredService;

	@Mock
	BCryptPasswordEncoder bCryptPasswordEncoder;

	@Mock
	UserRegisteredRepository userRegisteredRepository;


	@BeforeAll
	static void init(){
		log.info("init -> set up once");
	}


	@Test
	void createUserAndPasswordDoNotMatch(){

		UserCreationRequest userCreationRequest = new UserCreationRequest("dario", "password", "passwordDistinto");

		ResponseWrapper<UserRegistered> responseWrapper = userRegisteredService.save(userCreationRequest);

		Assertions.assertEquals(responseWrapper.getErrors().get(0), "The passwords do not match", "Compruebo contraseñas");
	}

	@Test
	void createUserAndPasswordEmpty(){

		UserCreationRequest userCreationRequest = new UserCreationRequest("dario", "", "passwordDistinto");

		ResponseWrapper<UserRegistered> responseWrapper = userRegisteredService.save(userCreationRequest);

		Assertions.assertEquals(responseWrapper.getErrors().get(0), "The username or password is empty", "Compruebo Campos");
	}

	@Test
	void createUserOK(){

		UserCreationRequest userCreationRequest = new UserCreationRequest("dario", "password", "password");

		Mockito.when(bCryptPasswordEncoder.encode(Mockito.any(String.class)))
				.thenReturn("$2a$10$8OiBiG4P55zLD5OOmcz4U.G5Ui.vTFSQxh2f33wlZOr.wBIAvfymO");

		Mockito.when(userRegisteredRepository.save(Mockito.any(UserRegistered.class)))
				.thenReturn(new UserRegistered(userCreationRequest));

		ResponseWrapper<UserRegistered> responseWrapper = userRegisteredService.save(userCreationRequest);

		Assertions.assertTrue(responseWrapper.isFinished_ok(), "Compruebo Creacion del usuario OK");
	}
*/

}
