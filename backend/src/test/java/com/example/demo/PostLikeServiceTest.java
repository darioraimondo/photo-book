package com.example.demo;


import com.example.demo.authentication.model.User;
import com.example.demo.dtos.ResponseWrapper;
import com.example.demo.exceptions.BusinessLogicException;
import com.example.demo.mocks.CustomMocks;
import com.example.demo.model.PostContent;
import com.example.demo.model.PostLike;
import com.example.demo.repositories.PostLikeRepository;
import com.example.demo.services.PostContentService;
import com.example.demo.services.PostLikeService;
import com.example.demo.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Pageable;

import java.util.Optional;


@Slf4j
@SpringBootTest
public class PostLikeServiceTest {

    @Mock
    private UserService userService;

    @Mock
    private PostContentService postContentService;

    @Mock
    PostLikeRepository postLikeRepository;

    @InjectMocks
    PostLikeService sut;

    @Test
    void UserLikesPostOK() {
        User userRegistered = CustomMocks.getUserMock();
        Mockito.when(userService.findByEmail(Mockito.any(String.class)))
                .thenReturn(Optional.of(userRegistered));

        PostContent postContent = new PostContent("texto de la imagen", "", null);
        Mockito.when(postContentService.findById(Mockito.any(Long.class))).thenReturn(Optional.of(postContent));

        PostLike postLike = new PostLike(postContent, userRegistered);
        Mockito.when(postLikeRepository.findByUserAndPostContent(Mockito.any(User.class), Mockito.any(PostContent.class)))
                .thenReturn(Optional.empty());

        Mockito.when(postLikeRepository.save(Mockito.any(PostLike.class))).thenReturn(postLike);

        ResponseWrapper<PostLike> responseWrapper = sut.likePost("dario", 1L);

        Assertions.assertNotNull(responseWrapper.getContent(), "Compruebo LIKE OK");
    }

    @Test
    void UserLikesPostAlreadyLiked() {
        User userRegistered = CustomMocks.getUserMock();
        Mockito.when(userService.findByEmail(Mockito.any(String.class)))
                .thenReturn(Optional.of(userRegistered));

        PostContent postContent = new PostContent("texto de la imagen", "", null);
        Mockito.when(postContentService.findById(Mockito.any(Long.class))).thenReturn(Optional.of(postContent));

        PostLike postLike = new PostLike(postContent, userRegistered);
        Mockito.when(postLikeRepository.findByUserAndPostContent(Mockito.any(User.class), Mockito.any(PostContent.class)))
                .thenReturn(Optional.of(postLike));

        Mockito.when(postLikeRepository.save(Mockito.any(PostLike.class))).thenReturn(postLike);

        Assertions.assertThrows(BusinessLogicException.class, () -> {
            sut.likePost("dario", 1L);
        });
    }

    @Test
    void UserLikesPostDoesNotExists() {
        User userRegistered = CustomMocks.getUserMock();
        Mockito.when(userService.findByEmail(Mockito.any(String.class)))
                .thenReturn(Optional.of(userRegistered));

        PostContent postContent = new PostContent("texto de la imagen", "", null);
        Mockito.when(postContentService.findById(Mockito.any(Long.class))).thenReturn(Optional.empty());

        PostLike postLike = new PostLike(postContent, userRegistered);
        Mockito.when(postLikeRepository.findByUserAndPostContent(Mockito.any(User.class), Mockito.any(PostContent.class)))
                .thenReturn(Optional.empty());

        Mockito.when(postLikeRepository.save(Mockito.any(PostLike.class))).thenReturn(postLike);

        Assertions.assertThrows(BusinessLogicException.class, () -> {
            sut.likePost("dario", 1L);
        });
    }
}
