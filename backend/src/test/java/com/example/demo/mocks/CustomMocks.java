package com.example.demo.mocks;

import com.example.demo.authentication.model.AuthProvider;
import com.example.demo.authentication.model.User;

public class CustomMocks {


    public static User getUserMock(){
        return new User(1L, "admin", "admin@demo.com", null, false, "$2a$10$LszZRMe617VUnn9jNRK/UefAdriHzf1osZAGg7n9VnnZy6CtgZo5O", AuthProvider.local, "");
    }
}
