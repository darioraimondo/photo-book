package com.example.demo;


import com.example.demo.authentication.model.User;
import com.example.demo.dtos.CreatePostRequest;
import com.example.demo.dtos.ResponseWrapper;
import com.example.demo.mocks.CustomMocks;
import com.example.demo.model.PostContent;
import com.example.demo.repositories.PostContentRepository;
import com.example.demo.services.PostContentService;
import com.example.demo.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Slf4j
@SpringBootTest
public class PostContentServiceTest {

    @Mock
    private UserService userService;

    @Mock
    private PostContentRepository postContentRepository;

    @InjectMocks
    private PostContentService postContentService;

    @Test
    void getAllPostsFromUserOK() {

        Pageable pageable = getPageableMock();

        List<PostContent> postContentList = new ArrayList<>();
        postContentList.add(new PostContent("texto de la imagen", "", null));
        Page<PostContent> postContentPage = new PageImpl(postContentList);

        Mockito.when(userService.findByEmail(Mockito.any(String.class)))
                .thenReturn(Optional.of(CustomMocks.getUserMock()));

        Mockito.when(postContentRepository.findByUser(
                Mockito.any(User.class),
                Mockito.any(Pageable.class))
        )
                .thenReturn(postContentPage);


        Page<PostContent> postContentResponse = postContentService.getAllPostsFromUserName("dario", pageable);

        Assertions.assertEquals(postContentResponse.getContent().get(0).getText(), "texto de la imagen", "Compruebo Campos");
    }


    @Test
    void createPostOk() {

        CreatePostRequest createPostRequest = new CreatePostRequest("texto de la imagen insertada", getMultiPartFileMock());

        PostContent postContent = new PostContent("texto de la imagen insertada", "", CustomMocks.getUserMock());

        Mockito.when(userService.findByEmail(Mockito.any(String.class))).thenReturn(Optional.of(CustomMocks.getUserMock()));
        Mockito.when(postContentRepository.save(Mockito.any(PostContent.class))).thenReturn(postContent);

        ResponseWrapper<PostContent> responseWrapper = postContentService.createPost("dario", createPostRequest);

        Assertions.assertEquals(responseWrapper.getContent().getText(), "texto de la imagen insertada", "Compruebo Campos");

    }


    private MultipartFile getMultiPartFileMock() {
        return new MultipartFile() {
            @Override
            public String getName() {
                return null;
            }

            @Override
            public String getOriginalFilename() {
                return null;
            }

            @Override
            public String getContentType() {
                return null;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public long getSize() {
                return 0;
            }

            @Override
            public byte[] getBytes() throws IOException {
                return new byte[0];
            }

            @Override
            public InputStream getInputStream() throws IOException {
                return null;
            }

            @Override
            public void transferTo(File dest) throws IOException, IllegalStateException {
            }
        };
    }

    private Pageable getPageableMock() {
        return new Pageable() {
            @Override
            public int getPageNumber() {
                return 0;
            }

            @Override
            public int getPageSize() {
                return 2;
            }

            @Override
            public long getOffset() {
                return 0;
            }

            @Override
            public Sort getSort() {
                return null;
            }

            @Override
            public Pageable next() {
                return null;
            }

            @Override
            public Pageable previousOrFirst() {
                return null;
            }

            @Override
            public Pageable first() {
                return null;
            }

            @Override
            public boolean hasPrevious() {
                return false;
            }
        };
    }
}
