package com.example.demo.model;

import com.example.demo.authentication.model.User;
import lombok.*;

import javax.persistence.*;


@Entity
@Getter
@Setter
@NoArgsConstructor
@ToString
public class PostLike extends CommonModel{

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;

    @ManyToOne
    private User user;

    @ManyToOne
    private PostContent postContent;


    public PostLike(PostContent postContent, User user) {
        this.user = user;
        this.postContent = postContent;
    }
}
