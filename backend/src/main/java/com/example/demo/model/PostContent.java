package com.example.demo.model;

import com.example.demo.authentication.model.User;
import javassist.bytecode.ByteArray;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;


@Entity
@Getter
@Setter
@NoArgsConstructor
@ToString
@Slf4j
public class PostContent extends CommonModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private User user;

    private String text;

    private byte[] image;


    public PostContent(String text, MultipartFile image, User user) {

        this.text = text;
        this.user = user;

        if (image != null) {
            try {
                this.image = image.getBytes();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public PostContent(String text, String image, User user) {
        this.text = text;
        this.user = user;
        this.image = Base64.getDecoder().decode(image.getBytes());
    }

}
