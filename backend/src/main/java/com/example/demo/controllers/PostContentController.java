package com.example.demo.controllers;

import com.example.demo.dtos.CreatePostRequest;
import com.example.demo.model.PostContent;
import com.example.demo.services.PostContentService;
import com.example.demo.services.PostLikeService;
import com.example.demo.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;


@Slf4j
@RestController
@RequestMapping("/post")
public class PostContentController {

    @Autowired
    UserService userService;

    @Autowired
    PostContentService postContentService;

    @Autowired
    PostLikeService postLikeService;

    @GetMapping
    public ResponseEntity<Page<PostContent>> getAllPosts(@PageableDefault(sort = "createdDate", direction = Sort.Direction.DESC) Pageable page){

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return ResponseEntity.ok().body(postContentService.getAllPostsFromUserName(auth.getName(), page));
    }


    @PostMapping(path = "/create", consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
    public ResponseEntity<?> createPost(@ModelAttribute CreatePostRequest createPostRequest){

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        return ResponseEntity.ok().body(postContentService.createPost(auth.getName(), createPostRequest));

    }

    @GetMapping("/like/{postId}")
    public ResponseEntity<?> likePost(@PathVariable Long postId){

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        return ResponseEntity.ok().body(postLikeService.likePost(auth.getName(), postId));

    }

    @GetMapping("/users")
    public ResponseEntity<?> getAllUsers(){

        return ResponseEntity.ok().body(userService.findAll());

    }


}
