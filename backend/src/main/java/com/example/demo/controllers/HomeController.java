package com.example.demo.controllers;

import com.example.demo.dtos.CreatePostRequest;
import com.example.demo.model.PostContent;
import com.example.demo.services.ExerciseService;
import com.example.demo.services.PostContentService;
import com.example.demo.services.PostLikeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;


@Slf4j
@RestController
public class HomeController {

    @Autowired
    ExerciseService exerciseService;

    @GetMapping("/")
    public ResponseEntity<?> getHomePage(){
        return ResponseEntity.ok().body("Welcome to the home page: photo-book");
    }

    @GetMapping("/streams")
    public ResponseEntity<?> getStreams(){
        return ResponseEntity.ok().body(exerciseService.playWithStreams());
    }

    @GetMapping("/maps")
    public ResponseEntity<?> getMaps(){
        return ResponseEntity.ok().body(exerciseService.playWithMaps());
    }

}
