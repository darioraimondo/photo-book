package com.example.demo.repositories;

import com.example.demo.authentication.model.User;
import com.example.demo.model.PostContent;
import com.example.demo.model.PostLike;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PostLikeRepository extends JpaRepository<PostLike, Long> {

    Optional<PostLike> findByUserAndPostContent(User user, PostContent postContent);
}
