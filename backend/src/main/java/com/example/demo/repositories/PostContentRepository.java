package com.example.demo.repositories;

import com.example.demo.authentication.model.User;
import com.example.demo.model.PostContent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostContentRepository extends JpaRepository<PostContent, Long> {


    Page<PostContent> findByUser(User user, Pageable page);
}
