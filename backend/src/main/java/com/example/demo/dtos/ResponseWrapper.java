package com.example.demo.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;


@Getter
@Setter
@ToString
public class ResponseWrapper<T> {

    private List<String> errors;
    private T content;

    public ResponseWrapper() {
        errors = new ArrayList<>();
        content = null;
    }

    public void addErrorMsg(String msg){
        errors.add(msg);
    }

}
