package com.example.demo.dtos;


import lombok.*;
import org.springframework.web.multipart.MultipartFile;

@NoArgsConstructor
@Getter
@Setter
@ToString
@AllArgsConstructor
public class CreatePostRequest {

    private String text;

    private MultipartFile image;
}
