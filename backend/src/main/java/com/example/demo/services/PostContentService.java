package com.example.demo.services;



import com.example.demo.authentication.model.User;
import com.example.demo.authentication.repository.UserRepository;
import com.example.demo.dtos.CreatePostRequest;
import com.example.demo.dtos.ResponseWrapper;
import com.example.demo.model.PostContent;
import com.example.demo.repositories.PostContentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
public class PostContentService {

    PostContentRepository postContentRepository;
    UserService userService;

    @Autowired
    PostContentService(PostContentRepository postContentRepository, UserService userService){
        this.postContentRepository = postContentRepository;
        this.userService = userService;
    }


    public Page<PostContent> getAllPostsFromUserName(String username, Pageable page){

        Optional<User> user = userService.findByEmail(username);

        if (user.isPresent()) {
            return postContentRepository.findByUser(user.get(), page);
        }

        return Page.empty();
    }


    public ResponseWrapper<PostContent> createPost(String username, CreatePostRequest createPostRequest){

        ResponseWrapper<PostContent> responseWrapper = new ResponseWrapper<>();
        Optional<User> userRegistered = userService.findByEmail(username);

        if(userRegistered.isPresent()) {
            PostContent postContent = new PostContent(createPostRequest.getText(), createPostRequest.getImage(), userRegistered.get());
            responseWrapper.setContent(postContentRepository.save(postContent));
            return responseWrapper;
        }

        return responseWrapper;
    }


    public Optional<PostContent> findById(Long postId) {
        return postContentRepository.findById(postId);
    }
}
