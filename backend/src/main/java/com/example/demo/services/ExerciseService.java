package com.example.demo.services;


import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;


@Service
@Slf4j
public class ExerciseService {

    List<String> stringList = new ArrayList<>();
    Map<Integer, String> map = new HashMap<>();

    public List<String> playWithStreams(){
        stringList.add("dario");
        stringList.add("gonzalo");
        stringList.add("leonardo");

        log.info("list.contains:" + stringList.contains("dario"));
        stringList.sort(Comparator.naturalOrder());

        //return nombres.stream().filter(i -> i.contains("e")).collect(Collectors.toList());
        return stringList.stream().filter(i -> i.contains("r")).sorted(Comparator.reverseOrder()).collect(Collectors.toList());
    }

    public Map<Integer, String> playWithMaps(){

        map.put(3, "leonardo");
        map.put(1, "dario");
        map.put(2, "gonzalo");

        log.info("containsKey: "+map.containsKey(1));
        log.info("containsValue; "+map.containsValue("dario"));

        map.forEach((k, v) -> log.info(k + " : " + v));
        return map;
    }

    public String playWithThreads(){
        Thread thread = new Thread();

        thread.start();


    }


}
