package com.example.demo.services;


import com.example.demo.authentication.model.User;
import com.example.demo.authentication.repository.UserRepository;
import com.example.demo.dtos.ResponseWrapper;
import com.example.demo.exceptions.BusinessLogicException;
import com.example.demo.model.PostContent;
import com.example.demo.model.PostLike;
import com.example.demo.repositories.PostContentRepository;
import com.example.demo.repositories.PostLikeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;
import java.util.Optional;


@Service
@Slf4j
public class PostLikeService {

    PostLikeRepository postLikeRepository;
    PostContentService postContentService;
    UserService userService;

    @Autowired
    PostLikeService(PostLikeRepository postLikeRepository, PostContentService postContentService, UserService userService) {
        this.postLikeRepository = postLikeRepository;
        this.postContentService = postContentService;
        this.userService = userService;
    }


    public ResponseWrapper<PostLike> likePost(String username, Long postId) {

        ResponseWrapper<PostLike> responseWrapper = new ResponseWrapper<>();

        Optional<User> userRegistered = userService.findByEmail(username);
        Optional<PostContent> mediaContent = postContentService.findById(postId);

        if (userRegistered.isPresent() && mediaContent.isPresent()) {
            Optional<PostLike> existingLike = postLikeRepository.findByUserAndPostContent(userRegistered.get(), mediaContent.get());

            if (!existingLike.isPresent()) {
                PostLike postLike = new PostLike(mediaContent.get(), userRegistered.get());
                responseWrapper.setContent(postLikeRepository.save(postLike));
                return responseWrapper;
            }

            throw new BusinessLogicException("User Has already liked that post");

        }

        throw new BusinessLogicException("The Post to like was not found on the db");
    }
}
