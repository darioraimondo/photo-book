package com.example.demo.services;


import com.example.demo.authentication.model.User;
import com.example.demo.authentication.repository.UserRepository;
import com.example.demo.dtos.CreatePostRequest;
import com.example.demo.dtos.ResponseWrapper;
import com.example.demo.model.PostContent;
import com.example.demo.repositories.PostContentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class UserService {

    UserRepository userRepository;

    @Autowired
    UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Optional<User> findByEmail(String username) {
        return userRepository.findByEmail(username);
    }

    public boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    public User save(User user) {
        return userRepository.save(user);
    }

    public List<User> findAll() {
        List<User> users = userRepository.findAll();

        return users.stream()
                .filter(u -> u.getName().contains("e"))
                .sorted(Comparator.comparing(User::getName).reversed())
                .collect(Collectors.toList());
    }

}
