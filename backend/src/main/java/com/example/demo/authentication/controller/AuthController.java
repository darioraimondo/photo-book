package com.example.demo.authentication.controller;

import com.example.demo.authentication.exception.BadRequestException;
import com.example.demo.authentication.model.AuthProvider;
import com.example.demo.authentication.model.User;
import com.example.demo.authentication.payload.ApiResponse;
import com.example.demo.authentication.payload.AuthResponse;
import com.example.demo.authentication.payload.LoginRequest;
import com.example.demo.authentication.payload.SignUpRequest;
import com.example.demo.authentication.repository.UserRepository;
import com.example.demo.authentication.security.TokenProvider;
import com.example.demo.authentication.services.AuthService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@Slf4j
@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private AuthService authService;

    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        log.info(loginRequest.toString());
        return ResponseEntity.ok().body(authService.authenticate(loginRequest));
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {

        ApiResponse apiResponse = authService.signUp(signUpRequest);
        return ResponseEntity.created(apiResponse.getLocation()).body(apiResponse);
    }

}
