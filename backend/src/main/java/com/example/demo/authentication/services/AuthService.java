package com.example.demo.authentication.services;




import com.example.demo.authentication.exception.BadRequestException;
import com.example.demo.authentication.model.AuthProvider;
import com.example.demo.authentication.model.User;
import com.example.demo.authentication.payload.ApiResponse;
import com.example.demo.authentication.payload.AuthResponse;
import com.example.demo.authentication.payload.LoginRequest;
import com.example.demo.authentication.payload.SignUpRequest;
import com.example.demo.authentication.security.TokenProvider;
import com.example.demo.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;


@Service
@Slf4j
public class AuthService {

    private final AuthenticationManager authenticationManager;

    private final TokenProvider tokenProvider;

    private final UserService userService;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public AuthService(AuthenticationManager authenticationManager,
                       TokenProvider tokenProvider,
                       UserService userService,
                       PasswordEncoder passwordEncoder){
        this.authenticationManager = authenticationManager;
        this.tokenProvider = tokenProvider;
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }

    public AuthResponse authenticate(LoginRequest loginRequest) {

        try {

            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            loginRequest.getEmail(),
                            loginRequest.getPassword()
                    )
            );
            SecurityContextHolder.getContext().setAuthentication(authentication);

            String token = tokenProvider.createToken(authentication);
            return new AuthResponse(token);
        }
        catch (BadCredentialsException e){
            log.error("Incorrect email or password.");
            throw new BadRequestException("Incorrect email or password.");
        }

    }

    public ApiResponse signUp(SignUpRequest signUpRequest) {


        if(userService.existsByEmail(signUpRequest.getEmail())) {
            throw new BadRequestException("Email address already in use.");
        }

        // Creating user's account
        User user = new User();
        user.setName(signUpRequest.getName());
        user.setEmail(signUpRequest.getEmail());
        user.setPassword(signUpRequest.getPassword());
        user.setProvider(AuthProvider.local);

        user.setPassword(passwordEncoder.encode(user.getPassword()));

        User result = userService.save(user);

        URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/user/me")
                .buildAndExpand(result.getId()).toUri();



        return new ApiResponse(true, "User registered successfully@", location);
    }
}
