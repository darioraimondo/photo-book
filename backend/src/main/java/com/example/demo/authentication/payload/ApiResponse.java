package com.example.demo.authentication.payload;

import lombok.Getter;
import lombok.Setter;

import java.net.URI;

@Getter
@Setter
public class ApiResponse {
    private boolean success;
    private String message;
    private URI location;

    public ApiResponse(boolean success, String message, URI location) {
        this.success = success;
        this.message = message;
        this.location = location;
    }

}
