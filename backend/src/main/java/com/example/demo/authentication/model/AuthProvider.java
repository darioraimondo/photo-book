package com.example.demo.authentication.model;

public enum  AuthProvider {
    local,
    facebook,
    google,
    github
}
