package com.example.demo.exceptions;


import com.example.demo.dtos.ResponseWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.NoSuchElementException;

@Slf4j
@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(
            value = {
                    BusinessLogicException.class
            }
    )
    protected ResponseEntity<Object> badRequest(RuntimeException ex, WebRequest request) {
        ResponseWrapper<String> bodyOfResponse = new ResponseWrapper<>();
        bodyOfResponse.addErrorMsg(ex.getMessage());
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> unhandledException(Exception ex, WebRequest request) {
        String bodyOfResponse = "UNHANDLED EXCEPTION";
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

}