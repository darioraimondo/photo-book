# Photo-book

### How to run the application

### Pre requirements:
* docker and docker compose installed
* npm installed

### Starting the backend:
1. Clone the project: 
   * `git clone https://gitlab.com/darioraimondo/photo-book.git` or
   * `git clone git@gitlab.com:darioraimondo/photo-book.git` 
2. Open a terminal and go to the backend root folder: `cd photo-boot/backend` 
3. Use docker-compose: `docker-compose up`
4. Open a second terminal and go the backend root folder: `cd photo-boot/backend`
5. Start the application: `mvnw spring-boot:run`
6. The backend will listen on: `localhost:8080` 


### Starting de frontend:
1. Open a terminal and go to the frontend root folder: `cd photo-boot/backend`
2. Install node modules: `npm install`
3. Start the application: `npm start`
4. the previous command will open a browser and display the frontend (it will take a while)
5. The frontend will listen on: `localhost:3000`


### Features:
* Easy to set up docker using docker-compose.
* PostgresSQL running inside docker container.
* Hibernate to manage entities and database interaction
* Test cases for each endpoint, covering successful and failure scenarios.  
* Backend secured using Spring Security + JWT.
* "get-all" endpoints support pagination.
* Swagger to handle Endpoint documentation. `http://localhost:8080/swagger-ui/`


### More data:
1. Altough the backend endpoints are ready to create, like and display posts, the frontend can only show them (for now)
2. There is a user pre-generated in order to view how posts are displayed.
3. Demo user credentials: `email: admin@demo.com` `password: password`

----

### Endpoints:
### The only endpoints available without authenticating are:
### `/auth/authenticate`
* Returns a token jwt for the user received in the body (if exists in the database)
* method: POST
* body (Json):
    `
  {
  "email": "admin@example.com",
  "password": "password"
  }
    `
### `/auth/signup`
* Registers a new user
* method: POST
* body (Json):
  `
  {
  "email": "string",
  "name": "string",
  "password": "string"
  } 
  `
### Endpoints that require jwt token _(see: /user/authenticate)_
### `/post?size=2&page=0&sort=createdDate,desc`
* Returns all the posts from logged user (jwt token owner).
* It supports pagination and sorting
* `size`, `page` and `sort` are optional, the api will use default values if missing.
* method: GET

### `/post/create`
* Creates a new post from logged user (jwt token owner)
* method: POST
* body (MultiPartForm):
  `image: MultiPartFile`
  `text: image description text`
  
### `/post/like/{post_id_to_like}`
* Adds the target post to the list of liked posts by the logged user (jwt token owner)
* method: GET

---

### Database information:
The database is PostgreSQL and it will run in a docker container.
* Port: 5435
* database name: docker
* database pass: docker
* database user: docker
* database schema:
  <!-- -->
  ![backend/images/database.png](backend/images/database.png "Database Diagram")

  
