import React from 'react'

export default function Post({id, image, text}) {

    const data = image

    return(
            <div className="post" key={id}>            
                <div>
                    <img src={`data:image/jpeg;base64,${data}`} alt="text" className="img-responsive" />
                </div>
                <span>{text}</span>    
            </div>
        )
    
}

