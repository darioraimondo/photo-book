import React from 'react'
import Post from './Post'


export default function Posts (postData){

    var postArray = postData.postData;

    if(postArray != null)
    {
        return postArray.map(item =>{
            //console.log("<Post id={"+item.id+"} image={"+item.image+"} text={"+item.text+"} />");
            return <Post key={item.id} image={item.image} text={item.text} />
        })
    }
    else{
        //console.log("POST DATA NULO");
    }
    return null;
   
}
