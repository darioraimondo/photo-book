import React, { Component } from 'react';
import './Profile.css';
import './post.css';
import Posts from './Posts';
import Alert from 'react-s-alert';
import { request } from '../../util/APIUtils'
import { API_BASE_URL} from '../../constants';



class Profile extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            options: {
                url: API_BASE_URL + "/post?size=8&page=0&sort=createdDate,desc",
            }
        };

        this.handlePostCall = this.handlePostCall.bind(this);
    }

    componentDidMount(){
        this.handlePostCall();
    }


    handlePostCall(){
        request(this.state.options)
        .then(response => {
            Alert.success("GETTING POSTS");

            this.setState({
                 postData: response.content
            });
                

        }).catch(error => {
            Alert.error((error && error.message) || 'Oops! Something went wrong GETTING POSTS. Please try again!');            
        });

    }

    render() {
        return (
            <div className="profile-container">
                <div className="container">
                    <div className="profile-info">
                        <div className="profile-avatar">
                        </div>
                        <div className="profile-name">
                        </div>
                    </div>
                    <div>
                        <Posts postData={this.state.postData} />
                    </div>
                </div>    
            </div>
        );
    }
}

export default Profile;